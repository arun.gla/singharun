---
date: 2023-05-15T10:58:08-04:00
description: "Kubernetes: Items to focus"
featured_image: "/icon-for-blog-28.jpg"
tags: ["Kubernetes"]
title: "Kubernetes: Items to focus"
---

50 Kubernetes items to focus:
- Use the latest version
- Use managed clusters
- Use multiple nodes
- Isolate nodes
- Use namespaces
- Use role-based access control
- Use third-party authentication provider
- Use official images
- Use smaller images
- Update images frequently
- Use a private registry
- Use declarative YAML files
- Use network policies
- Use a firewall
- Use process whitelisting
- Avoid privileged containers
- Use a read-only filesystem in containers
- Set resource requests and limits
- Use annotations and labels
- Use readiness and liveness probes
- Use logging and monitoring tools
- Automatically roll deployments
- Use autoscaling
- Turn on audit logging
- Monitor control plane components
- Monitor disk usage
- Monitor network traffic
- Have an alert system
- Externalise all configurations
- Lint manifest files
- Be proactive using Policy-as-Code
- Prefer stateless applications
- Avoid databases on Kubernetes
- Use a version control system
- Use GitOps
- Use Helm
- Lock down Kubelet
- Protect etcd
- Restrict API access
- Restrict SSH access
- Use Deployments instead of Pods
- Define the risk tolerance in code
- Run more than one replica
- Set disruption budgets
- Mount secrets as volumes
- Store secrets encrypted
- Set graceful shutdown
- Application logs to stdout and stderr
- Avoid sidecars for logging
- Include PVCs in Pod configuration
