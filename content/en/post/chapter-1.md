---
date: 2022-12-09T10:58:08-04:00
description: "Kubernetes: Lifecycle events"
featured_image: "/icon-for-blog-28.jpg"
tags: ["Kubernetes"]
title: "Kubernetes: How to add Handlers to Container Lifecycle Events"
---

Define postStart and preStop handlers 
In this exercise, you create a Pod that has one Container. The Container has handlers for the postStart and preStop events.

Here is the configuration file for the Pod:

```
apiVersion: v1
kind: Pod
metadata:
  name: lifecycle-demo
spec:
  containers:
  - name: lifecycle-demo-container
    image: nginx
    lifecycle:
      postStart:
        exec:
          command: ["/bin/sh", "-c", "echo Hello from the postStart handler > /usr/share/message"]
      preStop:
        exec:
          command: ["/bin/sh","-c","nginx -s quit; while killall -0 nginx; do sleep 1; done"]
```



