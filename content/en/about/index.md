---
title: "About"
description: "Solution Architect | Technical Lead | AWS | AZURE | Devops | Kubernetes"
featured_image: '/DevOps_from_Integration_to_Deployment.jpg'
menu:
  main:
    weight: 1
---
{{< figure src="/DevOps_from_Integration_to_Deployment.jpg" title="Solution Architect | Technical Lead | AWS | AZURE | Devops | Kubernetes" >}}

I have an MSc degree in Cloud Computing from Munster Technological University in Cork, Ireland.

An AWS Certified Solution architect with 16 years of IT experience creating Java applications and designing IT infrastructure & networking architecture. 
Expert in strategizing, cost optimization designing, and deploying cloud native and cloud agnostic applications.
Good experience in migrating existing applications to cloud. Breaking down monolithic application to microservices.

My expertise includes analyzing cloud usage patterns, identifying cost inefficiencies, and implementing optimization strategies that can reduce expenses while maintaining or even improving performance levels. I am proficient in using various cloud cost optimization tools, including AWS Cost Explorer and Azure Cost Management.

My expertise includes designing and implementing highly available, scalable, and fault-tolerant cloud architectures that meet business requirements and regulatory compliance.

Additionally, I have experience in developing cloud migration strategies and implementing best practices for cloud security and cost optimization. I am proficient in using various AWS services, including EC2, S3, RDS, Lambda, CloudFormation, and CloudTrail.

I have earned several AWS certifications, including AWS Certified Solutions Architect - Associate and Professional. My educational qualifications include a Bachelor's degree in electronics and a Master's degree in Cloud Computing.

I led a team of AWS Solution Architects and successfully delivered several high-profile projects for different companies. I am a skilled communicator, able to work with stakeholders at all levels to translate business requirements into technical solutions.

Expert in strategizing, designing, and deploying innovative and complete cloud security architecture. Skilled in AWS/AZURE, Java/JEE, Microservices, Docker JSP, SQL, jQuery, Bootstrap, Servlets, Hibernate, and Spring Framework.
