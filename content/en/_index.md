---
title: "Hi, I am Arun"

description: "Solution Architect | Technical Lead | AWS | AZURE | Devops | Kubernetes"
cascade:
  featured_image: '/BG2.jpg'
---

`I am an AWS Certified Solution architect with 16 years of IT experience creating Java applications and designing IT infrastructure & networking architecture. `

Expert in strategizing, cost optimization designing, and deploying cloud native and cloud agnostic applications.
Good experience in **migrating existing applications to cloud**. Breaking down **monolithic** application to **microservices**.

My expertise includes analyzing cloud usage patterns, **identifying cost inefficiencies**, and implementing optimization strategies that can reduce expenses while maintaining or even improving performance levels. I am proficient in using various cloud **cost optimization** tools, including AWS Cost Explorer and Azure Cost Management.

My expertise includes designing and **implementing highly available, scalable, and fault-tolerant cloud architectures** that meet business requirements and regulatory compliance. Ensuring security and compliance with industry standards such as **PCI-DSS**, **HIPAA**, and **GDPR**

Additionally, I have experience in developing cloud migration strategies and implementing best practices for cloud security and cost optimization. I am proficient in using various AWS services, including **EKS, VPC, VPN, EC2, S3, RDS, Lambda, CloudFront , CloudFormation, and CloudTrail**.

I led a team of AWS Solution Architects and successfully delivered several high-profile projects for different companies. I am a skilled communicator, able to work with stakeholders at all levels to translate business requirements into technical solutions.

I have earned several AWS certifications, including AWS Certified Solutions Architect - Associate and Professional. My educational qualifications include a Bachelor's degree in electronics and a Master's degree in **Cloud Computing**.

Expert in strategizing, designing, and deploying innovative and complete cloud security architecture. Skilled in **AWS/AZURE, Kubernetes, Java/JEE, Microservices, Docker JSP, SQL, jQuery, Bootstrap, Servlets, Hibernate, and Spring Framework**.
